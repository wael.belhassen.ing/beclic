package org.audensiel.application.service

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.mockk.*
import io.mockk.CapturingSlot
import io.mockk.Runs
import org.audensiel.domain.entity.PlayerEntity
import org.audensiel.domain.repository.PlayerRepository
import java.util.*

class PlayerServiceImplTest : StringSpec() {

    val playerRepository = mockk<PlayerRepository>()
    val playerService = PlayerServiceImpl(playerRepository)

    init {
        "Should create player" {
            //given
            val playerSlot = CapturingSlot<PlayerEntity>()
            every { playerRepository.create(capture(playerSlot)) } just Runs
            //when
            playerService.create(login = "Wael", idCompetition = "Pocker Tournament")
            //then
            verify(exactly = 1) { playerRepository.create(allAny()) }
            UUID.fromString(playerSlot.captured.id).toString() shouldBe playerSlot.captured.id
            playerSlot.captured.login shouldBe "Wael"
            playerSlot.captured.idCompetition shouldBe "Pocker Tournament"
            playerSlot.captured.score shouldBe 0
        }

        "Should failed creating player with empty login" {
            //given
            every { playerRepository.create(any()) } just Runs
            //when
            playerService.create(login = "", idCompetition = "")
            //then
        }

        "Should call deleteAll players on repository" {
            //when
            every { playerRepository.deleteAll() } just Runs
            playerService.deleteAll()
            //then
            verify(exactly = 1) { playerRepository.deleteAll() }
        }

        "Should update player" {
            //when
            val playerSlot = CapturingSlot<PlayerEntity>()
            every { playerRepository.update(capture(playerSlot)) } just Runs
            every { playerRepository.findById(any()) } returns
                    PlayerEntity(id=UUID.randomUUID().toString(), login = "Nelly", score = 25, idCompetition = "BlackJaque")
            //given
            playerService.updateScore(UUID.randomUUID().toString(), 10)
            //then
            verify(exactly = 1) { playerRepository.update(any()) }
            playerSlot.captured.score shouldBe 10
        }
    }

}