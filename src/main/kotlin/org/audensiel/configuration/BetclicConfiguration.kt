package org.audensiel.configuration

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration


class BetclicConfiguration(@JsonProperty("database") val database: DbConfiguration) : Configuration() {

}

class DbConfiguration(@JsonProperty("region") val region: String,
                      @JsonProperty("endpoint") val endpoint: String,
                      @JsonProperty("pagesize") val pagesize: Int
)