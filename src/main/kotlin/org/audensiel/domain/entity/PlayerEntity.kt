package org.audensiel.domain.entity

import java.time.Instant

data class PlayerEntity (
    val id: String,
    val idCompetition: String,
    val login: String,
    val score: Int = 0,
    val creationDate: Instant = Instant.now(),
    val modificationDate: Instant = Instant.now()
) {
    fun updateScore(score: Int): PlayerEntity {
        return copy(score = score)
    }
}