package org.audensiel.domain.repository

import org.audensiel.domain.entity.PlayerEntity

interface PlayerRepository {
    fun create(player: PlayerEntity): Unit
    fun deleteAll(): Unit
    fun findAll(idCompetition: String, sortAsc: Boolean): List<PlayerEntity>
    fun findById(id: String): PlayerEntity
    fun findRank(player: PlayerEntity): Int
    fun update(newPlayer: PlayerEntity)
}