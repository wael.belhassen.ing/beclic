package org.audensiel.application.main

import io.dropwizard.Application
import io.dropwizard.setup.Environment
import org.audensiel.configuration.BetclicConfiguration
import org.audensiel.infrastructure.injection.component.DaggerBetclicComponent
import org.audensiel.infrastructure.injection.module.DynamoDbModule

fun main(args: Array<String>) {
    MainApp().run(*args)
}

class MainApp : Application<BetclicConfiguration>() {
    /**
     * When the application runs, this is called after the [ConfiguredBundle]s are run. Override it to add
     * providers, resources, etc. for your application.
     *
     * @param configuration the parsed [Configuration] object
     * @param environment   the application's [Environment]
     * @throws Exception if something goes wrong
     */
    override fun run(configuration: BetclicConfiguration?, environment: Environment?) {
        // Dagger graph object initialisation
        val component = DaggerBetclicComponent.builder()
            .dynamoDbModule(DynamoDbModule(configuration = configuration!!.database))
            .build()

        // Resources registration
        environment?.jersey()?.register(component.playerResource())

        //HealthCheck Registration
        environment?.healthChecks()?.register("dynamoDb", component.dynamoDbHealthCheck())
    }
}