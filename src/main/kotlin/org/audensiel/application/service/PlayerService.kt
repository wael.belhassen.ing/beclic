package org.audensiel.application.service

import org.audensiel.application.dto.PlayerDto
import org.audensiel.application.dto.PlayerInfoDto

interface PlayerService {

    /**
     * Create a player with login [login] for the competition [idCompetition]
     *
     * @param login
     * @param idCompetition
     */
    fun create(login: String, idCompetition: String): Unit

    /**
     * Delete all players for all competitions
     */
    fun deleteAll(): Unit

    /**
     * Return a list containing all the players for the competition [idCompetition].
     *
     * @param idCompetition
     * @param sortAsc should the players be sorted by their score in ascending order
     *
     * @return A list of players sorted by their score
     */
    fun findAll(idCompetition: String, sortAsc: Boolean): Iterable<PlayerDto>

    /**
     * Return a player identified by [id]
     *
     * @param id player's id
     */
    fun findById(id: String): PlayerInfoDto

    /**
     * Update the score of the player identified by [id] with the given score [score]
     *
     * @param idPlayer
     * @param score
     */
    fun updateScore(idPlayer: String, score: Int)
}