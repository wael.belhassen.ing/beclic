package org.audensiel.application.service

import org.audensiel.application.dto.PlayerDto
import org.audensiel.application.dto.PlayerInfoDto
import org.audensiel.domain.entity.PlayerEntity
import org.audensiel.domain.repository.PlayerRepository
import java.util.*
import javax.inject.Inject


class PlayerServiceImpl  @Inject constructor(val playerRepository: PlayerRepository) : PlayerService{
    /**
     * Create a player with login [login] for the competition [idCompetition]
     *
     * @param login
     * @param idCompetition
     */
    override fun create(login: String, idCompetition: String) {
        playerRepository.create(
            PlayerEntity(
            id= UUID.randomUUID().toString(),
            idCompetition = idCompetition,
            login = login)
        )
    }

    /**
     * Delete all players for all competitions
     */
    override fun deleteAll() {
        playerRepository.deleteAll()
    }

    /**
     * Return a list containing all the players for the competition [idCompetition].
     *
     * @param idCompetition
     * @param sortAsc should the players be sorted by their score in ascending order
     *
     * @return A list of players sorted by their score
     */
    override fun findAll(idCompetition: String, sortAsc: Boolean): Iterable<PlayerDto> {
        return playerRepository.findAll(idCompetition, sortAsc).map { mapper(it) };
    }

    /**
     * Return a player identified by [id]
     *
     * @param id player's id
     */
    override fun findById(id: String): PlayerInfoDto {
        val foundPlayer = playerRepository.findById(id)
        val rank:Int = playerRepository.findRank(foundPlayer)
        return mapper(foundPlayer, rank)
    }

    /**
     * Update the score of the player identified by [id] with the given score [score]
     *
     * @param idPlayer
     * @param score
     */
    override fun updateScore(idPlayer: String, score: Int) {
        val player = playerRepository.findById(idPlayer)
        playerRepository.update(player.updateScore(score))
    }

    private fun mapper(p: PlayerEntity) = PlayerDto(p.id, p.login, p.score)
    private fun mapper(p: PlayerEntity, rank:Int) = PlayerInfoDto(p.id, p.login, p.score,rank)

}