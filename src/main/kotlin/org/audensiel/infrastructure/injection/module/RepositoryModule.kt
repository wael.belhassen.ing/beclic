package org.audensiel.infrastructure.injection.module

import dagger.Binds
import dagger.Module
import org.audensiel.domain.repository.PlayerRepository
import org.audensiel.domain.repository.PlayerRepositoryImpl
import javax.inject.Singleton

@Module
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindPlayerRepository(repository: PlayerRepositoryImpl): PlayerRepository
}