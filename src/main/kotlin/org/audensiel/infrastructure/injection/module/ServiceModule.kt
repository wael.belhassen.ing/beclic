package org.audensiel.infrastructure.injection.module

import dagger.Binds
import dagger.Module
import org.audensiel.application.service.PlayerService
import org.audensiel.application.service.PlayerServiceImpl
import javax.inject.Singleton

@Module
abstract class ServiceModule {
    @Binds
    @Singleton
    abstract fun bindPlayerService(playerService: PlayerServiceImpl): PlayerService
}