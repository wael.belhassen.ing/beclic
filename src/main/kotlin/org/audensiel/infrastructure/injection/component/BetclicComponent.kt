package org.audensiel.infrastructure.injection.component

import dagger.Component
import org.audensiel.infrastructure.database.DynamoDbHealthCheck
import org.audensiel.endpoint.PlayerEndpoint
import org.audensiel.infrastructure.injection.module.DynamoDbModule
import org.audensiel.infrastructure.injection.module.RepositoryModule
import org.audensiel.infrastructure.injection.module.ServiceModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    RepositoryModule::class,
    ServiceModule::class,
    DynamoDbModule::class
])
interface BetclicComponent {
    fun playerResource(): PlayerEndpoint

    fun dynamoDbHealthCheck(): DynamoDbHealthCheck

    @Component.Builder
    interface Builder {
        fun dynamoDbModule(dynamoDbModule: DynamoDbModule): Builder

        fun build(): BetclicComponent
    }
}