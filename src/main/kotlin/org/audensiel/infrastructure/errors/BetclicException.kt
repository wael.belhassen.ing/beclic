package org.audensiel.infrastructure.errors

abstract class BetclicException (val code: ErrorCodeEnum, val msg: String): Exception(msg)  {
}