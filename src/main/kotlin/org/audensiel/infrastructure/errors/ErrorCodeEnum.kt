package org.audensiel.infrastructure.errors

import javax.ws.rs.core.Response

/**
 * Enum class for error code.
 * TECNICAL prefix stands for Technical
 * BUSINESS prefix stands for Business
 */
enum class ErrorCodeEnum {
    TECNICAL_CONNECTION_ERROR,
    TECNICAL_DB_ACTION_ERROR,
    BUSINESS_DATA_ALREADY_EXIST,
    BUSINESS_NOT_FOUND;

    fun response(): Response.ResponseBuilder {
        val builder = when (this) {
            BUSINESS_DATA_ALREADY_EXIST -> Response.status(Response.Status.CONFLICT)
            BUSINESS_NOT_FOUND -> Response.status(Response.Status.NOT_FOUND)
            TECNICAL_DB_ACTION_ERROR -> Response.status(Response.Status.INTERNAL_SERVER_ERROR)
            TECNICAL_CONNECTION_ERROR -> Response.status(Response.Status.SERVICE_UNAVAILABLE)
        }
        return builder
    }
}