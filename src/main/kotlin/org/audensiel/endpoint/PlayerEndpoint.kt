package org.audensiel.endpoint

import org.audensiel.application.service.PlayerService
import org.audensiel.infrastructure.errors.BetclicException
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/betclic/player")
@Produces(MediaType.APPLICATION_JSON)
class PlayerEndpoint @Inject constructor(val playerService: PlayerService)  {

    @POST
    fun create(@QueryParam("competition") idCompetition: String, @QueryParam("login") login: String): Response {
        return handleResponse { playerService.create(login, idCompetition) }
    }

    @PUT
    fun update(@PathParam("idPlayer") idPlayer: String, @QueryParam("score") score: Int): Response {
        return handleResponse { playerService.updateScore(idPlayer, score) }
    }

    @GET
    fun findAll(@QueryParam("competition") idCompetition: String, @QueryParam("sortAsc") sortAscParam: Boolean?): Response {
        val sortAsc: Boolean = sortAscParam ?: false
        return handleResponse { playerService.findAll(idCompetition, sortAsc) }
    }

    @DELETE
    fun deleteAll(): Response {
        return handleResponse { playerService.deleteAll() }
    }

    @GET
    @Path("/{idPlayer}")
    fun findPlayer(@PathParam("idPlayer") idPlayer: String): Response {
        return handleResponse { playerService.findById(idPlayer) }
    }

    private fun <T> handleResponse(call: () -> T): Response {
        try {
            val callResponse:T = call()
            var response: Response.ResponseBuilder

            if(callResponse == Unit) {
                response = Response.noContent()
            }else {
                response = Response.ok(callResponse)
            }

            return response.build()
        } catch (e: BetclicException) {
            return e.code.response()
                .entity(e.msg)
                .build()
        }
    }
}